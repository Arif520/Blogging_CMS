<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([

            'site_name' => "Laravel's Blog",

            'address'=>'Chittagong , Bangladesh',

            'contact_number' =>'+880 16733 48095',

            'contact_email'=>'aparif0@gmail.com'

        ]);
    }
}
